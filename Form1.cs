using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Psych_Rechentest_GUI
{
    public partial class Psych_Rechentest : Form
    {
        public Psych_Rechentest()
        {
            InitializeComponent();
            ToolTip tp = new ToolTip();
            tp.SetToolTip(cmd_berechnen, "berechnen");
            tp.SetToolTip(cmd_clear, "Inhalte löschen");
            tp.SetToolTip(cmd_end, "Programm beenden");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_zahl1.Text = txt_zahl2.Text = txt_output.Text = null;
            txt_zahl1.Focus();
        }

        private void cmd_berechnen_Click(object sender, EventArgs e)
        {
            // Variablen
            double zahl1, zahl2, ergebnis1, ergebnis2;
            try
            {
                zahl1 = Convert.ToDouble(txt_zahl1.Text);
                zahl2 = Convert.ToDouble(txt_zahl2.Text);
                if (zahl1 > zahl2)
                {
                    ergebnis1 = zahl1 - zahl2;
                    txt_output.Text = ergebnis1.ToString("F2");
                }
                else
                    if (zahl1 < zahl2)
                    {
                        ergebnis2 = zahl1 + zahl2;
                        txt_output.Text = ergebnis2.ToString("F2");
                    }
                    else
                        txt_output.Text = "Dass beide Zahlen gleich sind, ist nicht vorgesehen.";
                        txt_zahl1.Text = txt_zahl2.Text = null;
                        txt_zahl1.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                    txt_zahl1.Text = txt_zahl2.Text = txt_output.Text = null;
                    txt_zahl1.Focus();
            }
        }
    }
}
